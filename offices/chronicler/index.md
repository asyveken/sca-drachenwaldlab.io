---
title: Kingdom Chronicler
excerpt: Recording the kingdom's events for posterity
toc: true
toc_label: Contents
---


# What a Chronicler does

The kingdom Chronicler is responsible for all official kingdom publications.

The kingdom chronicler:

* manages the [kingdom calendar (where you publish event announcements)]({{ site.baseurl }}{% link events/calendar.html %})
* publishes the [kingdom newsletter, the <em>Dragon's Tale</em>.]({{ site.baseurl }}{% link offices/chronicler/kingdom-newsletter.md %})
* encourages [local branches to keep newsletters](#local-chroniclers)
* [reports quarterly to the Society Chronicler](#policies-and-reports), as part of the requirements for Society for Creative Anachronism Inc

# Event Announcements

For an event to be "official" for business like awards, and proclamation of laws, you must publish the event on the Kingdom Calendar and send an announcement to the [_Dragon's Tale_]({{ site.baseurl }}{% link offices/chronicler/kingdom-newsletter.md %}).

* [Request a date on the kingdom calendar]({{ site.baseurl }}{% link events/calendar-add.html %})
* [Event announcement guidelines]({{ site.baseurl }}{% link offices/chronicler/guidelines-event-announcement.md %})
* [Make sure your event is official]({ site.baseurl }}{% link offices/chronicler/make-event-official.md %})

# Kingdom Newsletter

SCA Inc and SCA affiliate members: read _Dragon's Tale_, and every other kingdom's newsletter, online! 

Visit [SCA membership portal](https://members.sca.org/apps/#SignIn) and log in with the same login information that you use to renew your membership.

Affiliate members: From the membership portal, log in with the username "affiliate" and password "affiliate", all lowercase. Look for Drachenwald's newsletter.

If you have any difficulties or questions, please email <script type="text/javascript">document.write(String.fromCharCode(60,97,32,104,114,101,102,61,39,109,97,105,108,116,111,58,109,101,109,98,101,114,115,104,105,112,64,115,99,97,46,111,114,103,39,62,109,101,109,98,101,114,115,104,105,112,64,115,99,97,46,111,114,103,60,47,97,62));</script>. Include your modern name, membership number, and a brief explanation of the problem you're experiencing.

## Release Forms

[Release forms]({{ site.baseurl }}{% link offices/chronicler/release-forms.md %}) are required for the chronicler to publish original articles and artwork, and from persons appearing on photographs.

# Local Chroniclers

Local branches are encouraged to have a newsletter, though it is only <em>required</em>for Baronies and above.


* [Guidelines for local chroniclers]({{ site.baseurl }}{% link offices/chronicler/guidelines-local-chroniclers.md %})
* [William Blackfox awards](https://www.sca.org/chronicler/)

The Society Chronicler selects winners of the William Blackfox awards each year, to recognise excellence in local newsletters and their contributors. Each year, the kingdom chronicler nominates local newsletters, articles and artwork produced within the kingdom for the various awards.

# Policies and Reports

* [Kingdom chronicler policies]({{ site.baseurl }}{% link offices/chronicler/chronicler-policies.md %})
* [Template for quarterly reports]({{ site.baseurl }}{% link offices/chronicler/report-template.md %})


{% include officer-contacts.html %}