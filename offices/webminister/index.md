---
title: Office of the Web Minister
excerpt: Who keeps this online circus running?

---

## Kingdom web artificers

These folks volunteer to work on the kingdom website.

* Sela de la Rosa
* Genevieve la flechiere
* Aodh O Siadhail 
* Lydia von Are
* Milada von Schnecken
* Marcella di Cavellino

## Reporting for principality and local webministers

Mostly, Yda would like to hear from web ministers and would like to know if there's anything you're running into that kingdom web artificers can help with. Beyond that she appreciates getting an update on major things planned or that happened.  

## Kingdom site issues

Send a report to the Gitlab issues tracker: <a href="https://gitlab.com/sca-drachenwald/sca-drachenwald.gitlab.io/-/issues/new"> 


## Updating information on the website
* [Update officer information](https://forms.gle/Xm7bCu7nkq5uMU5Z6)
* [Adding a chartered group to the website]({{ site.baseurl}}{% link offices/webminister/content-policy.md %}) 

{% include officer-contacts.html %}